# LBL Muon Collider Workspace

Collection of packages for Muon Collider studies done at LBL.

## Repository Structure
- `exts/` External packages not included with the ILC framework.
- `packages/` All non-standard packages linked using git submodules.
- `sim/` Configuration files for generating events.

## Setup Instructions

### Container
All commands should be run inside the `gitlab-registry.cern.ch/berkeleylab/muoncollider/muoncollider-docker/mucoll-ilc-framework:1.5.1-centos8` container.

#### Singularity
```bash
singularity shell --cleanenv docker://gitlab-registry.cern.ch/berkeleylab/muoncollider/muoncollider-docker/mucoll-ilc-framework:1.5.1-centos8
```

#### Shifter
```bash
shifter --image gitlab-registry.cern.ch/berkeleylab/muoncollider/muoncollider-docker/mucoll-ilc-framework:1.5.1-centos8 /bin/bash
```

### Build Instructions
Run the following commands from inside your `mucoll-ilc-framework:1.5-centos8` container. The same commands will also work with a local installation of the ILC software, with the exception of the first line.
```bash
source /opt/ilcsoft/muonc/init_ilcsoft.sh # Setup ILC software
cmake -S . -B build 
cmake --build build
```

### Setup Script
The included `setup.sh` script is useful for defining all paths for the binaries built by the workspace. At the current stage, it setups the following:
- ILC software via `init_ilcsoft.sh`
- External binaries/libraries found in `exts`.
- Add all package libraries to `MARLIN_DLL`.
- Export `MYBUILD` variable with absolute path the build directory.
- Export `MYWORKSPACE` variable with absolute path the workspace directory.

Run the following at the start of every session. It has an optional argument to the build directory and is set to `build/` by default.
```bash
source setup.sh [build]
```

## Example Commands

Run the following commands from your run directory.

### Exporting Geometry to TGeo
```bash
${MYBUILD}/packages/ACTSMCC/dd2tgeo ${MYWORKSPACE}/data/MuColl_v1_mod0/MuColl_v1_mod0.xml MuColl_v1_mod0.root
```

### Event Simulation
```bash
ddsim --steeringFile ${MYBUILD}/data/sim_steer_muonGun_MuColl_v1.py
```

### Dump Events Into CSV Files
```bash
Marlin ${MYBUILD}/packages/ACTSTuple/example/actstuple_steer.xml --global.LCIOInputFiles=/path/to/events.slcio
```

### Run Tracking with ACTS
The truth tracking example.
```bash
Marlin ${MYBUILD}/packages/ACTSTracking/example/actstruth_steer.xml --global.LCIOInputFiles=/path/to/events.slcio
```

The CKF tracking example (truth-based seeds).
```bash
Marlin ${MYBUILD}/packages/ACTSTracking/example/actsckf_steer.xml --global.LCIOInputFiles=/path/to/events.slcio
```

The CKF tracking example (triplet seeds).
```bash
Marlin ${MYBUILD}/packages/ACTSTracking/example/actsseed_steer.xml --global.LCIOInputFiles=/path/to/events.slcio
```

## Modified Geometry Destription
The workspace contains a modified geometry description that makes it usable with the ACTS tracking framework. The modification are to logical description only and do not affect the physical properties of the detector.

### lcgeo Modifications
The sensitive parts of the detector have been renamed from `component#` to `sensor#`. This makes it easier to identify the sensor inside the TGeo geometry used by ACTS.

The modified `lcgeo` package is stored under `exts/`.

### XML Modifications
The inner tracker needs to be split into two parts to prevent overlap in the cylindrical bounding volumes. The modified geometry, `data/MuColl_v1_mod0`, adds a `OuterInnerTracker` subdetector with the outer layers in the inner tracker.
