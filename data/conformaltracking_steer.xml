<?xml version="1.0" encoding="us-ascii"?>
<!--
?xml-stylesheet type="text/xsl"
href="http://ilcsoft.desy.de/marlin/marlin.xsl"?
-->
<!-- ?xml-stylesheet type="text/xsl" href="marlin.xsl"? -->

<marlin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://ilcsoft.desy.de/marlin/marlin.xsd">

  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  <!-- == List of processors to execute                                                                      == -->
  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->

  <execute>
    <!-- ========== setup  ========== -->
    <processor name="MyAIDAProcessor"/>
    <processor name="EventNumber" />
    <processor name="Config" />

    <!-- ==========  Geometry initialization  ========== -->
    <processor name="InitDD4hep"/>

    <!-- ==========  Tracker Digitization  ========== -->
    <processor name="VXDBarrelDigitiser"/>
    <processor name="VXDEndcapDigitiser"/>
    <processor name="InnerPlanarDigiProcessor"/>
    <processor name="InnerEndcapPlanarDigiProcessor"/>
    <processor name="OuterPlanarDigiProcessor"/>
    <processor name="OuterEndcapPlanarDigiProcessor"/>

    <!-- ==========  Tracking  ========== -->
    <!-- At the moment the name of the final track collection for the MyTruthTrackFinder and MyExtrToTracker processors is the same, 
         so that users can use this example to run easily both the cheater track pattern recognition (still the default for many tasks) 
         or the real one (under final tests) -->
    <if condition="Config.TrackingTruth">
      <processor name="MyTruthTrackFinder"/>
    </if>
    <if condition="Config.TrackingConformal">
      <processor name="ConformalTrackingILC"/>
      <processor name="ClonesAndSplitTracksFinder"/>
    </if>
    <processor name="Refit" />
    <processor name="MyTrackTruth"/>

    <!-- ==========  Output  ========== -->
    <processor name="MergeHits" />
    <processor name="MyCreateTrackRelations" />
    <processor name="MyLCParquet" />
  </execute>


  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  <!-- == Global setup                                                                                       == -->
  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->

  <processor name="Config" type="CLICRecoConfig" >
    <parameter name="Verbosity" options="DEBUG0-9,MESSAGE0-9,WARNING0-9,ERROR0-9,SILENT"> DEBUG7  </parameter>
    <!--Which option to use for Overlay: False, BIB. Then use, e.g., Config.OverlayFalse in the condition-->
    <parameter name="Overlay" type="string">False</parameter>
    <!--Possible values and conditions for option Overlay-->
    <parameter name="OverlayChoices" type="StringVec">False BIB</parameter>
    <!--Which option to use for Tracking: Truth, ConformalPlusExtrapolator, Conformal. Then use, e.g., Config.TrackingTruth in the condition-->
    <parameter name="Tracking" type="string">Conformal</parameter>
    <!--Possible values and conditions for option Tracking-->
    <parameter name="TrackingChoices" type="StringVec">Truth Conformal</parameter>
    <!--Which option to use for VertexUnconstrained: ON, OFF. Then use, e.g., Config.VertexUnconstrainedOFF in the condition-->
    <parameter name="VertexUnconstrained" type="string">OFF </parameter>
    <!--Possible values and conditions for option Tracking-->
    <parameter name="VertexUnconstrainedChoices" type="StringVec">ON OFF  </parameter>
    <!--verbosity level of this processor ("DEBUG0-4,MESSAGE0-4,WARNING0-4,ERROR0-4,SILENT")-->
    <parameter name="Verbosity" options="DEBUG0-4,MESSAGE0-4,WARNING0-4,ERROR0-4,SILENT"> MESSAGE  </parameter> 
  </processor>

  <processor name="EventNumber" type="Statusmonitor">
    <parameter name="HowOften" type="int">1 </parameter>
    <parameter name="Verbosity" type="string"> MESSAGE </parameter>
  </processor>

  <processor name="MyAIDAProcessor" type="AIDAProcessor">
    <parameter name="FileName" type="string">
      lctuple_conformaltracking
    </parameter>
    <parameter name="FileType" type="string">root</parameter>
  </processor>

  <global>
    <parameter name="LCIOInputFiles">
      muonGun_sim.slcio
    </parameter>
    <parameter name="MaxRecordNumber" value="-1" />
    <parameter name="SkipNEvents" value="0" />
    <parameter name="SupressCheck" value="false" />
    <parameter name="Verbosity"	options="DEBUG0-4,MESSAGE0-4,WARNING0-4,ERROR0-4,SILENT">MESSAGE0</parameter>
  </global>

  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  <!-- == DD4hep configuration                                                                               == -->
  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  
  <processor name="InitDD4hep" type="InitializeDD4hep">
    <!--InitializeDD4hep reads a compact xml file and initializes the dd4hep::Detector object-->
    <!--Name of the DD4hep compact xml file to load-->
    <parameter name="DD4hepXMLFile" type="string">
      /opt/ilcsoft/muonc/detector-simulation/geometries/MuColl_v1/MuColl_v1.xml
    </parameter>
    <!--Alternate name for EncodingStringParameterName-->
    <!--If given, the Compact File parameter of that name will be used as argument to LCTrackerCellID::set_encoding_string()-->
    <parameter name="EncodingStringParameterName" type="string"> GlobalTrackerReadoutID  </parameter>
    <!--verbosity level of this processor ("DEBUG0-4,MESSAGE0-4,WARNING0-4,ERROR0-4,SILENT")-->
    <!--parameter name="Verbosity" type="string">DEBUG </parameter-->
  </processor>

  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  <!-- == Tracker Digitizer configuration                                                                    == -->
  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->

  <processor name="VXDBarrelDigitiser" type="DDPlanarDigiProcessor">
    <parameter name="SubDetectorName" type="string"> Vertex </parameter>
    <parameter name="IsStrip" type="bool">false </parameter>
    <parameter name="ResolutionU" type="float"> 0.005 </parameter>
    <parameter name="ResolutionV" type="float"> 0.005 </parameter>
    <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> VertexBarrelCollection </parameter>
    <parameter name="SimTrkHitRelCollection" type="string" lcioOutType="LCRelation"> VBTrackerHitsRelations </parameter>
    <parameter name="TrackerHitCollectionName" type="string" lcioOutType="TrackerHitPlane"> VBTrackerHits </parameter>
    <parameter name="ResolutionT" type="FloatVec"> 0.03  </parameter>
    <parameter name="UseTimeWindow" type="bool"> true </parameter>
    <parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
    <parameter name="TimeWindowMin" type="float"> -0.09 </parameter>
    <parameter name="TimeWindowMax" type="float"> 0.15 </parameter>
    <parameter name="Verbosity" type="string"> WARNING </parameter>
  </processor>

  <processor name="VXDEndcapDigitiser" type="DDPlanarDigiProcessor">
    <parameter name="SubDetectorName" type="string"> Vertex </parameter>
    <parameter name="IsStrip" type="bool">false </parameter>
    <parameter name="ResolutionU" type="float"> 0.005 </parameter>
    <parameter name="ResolutionV" type="float"> 0.005 </parameter>
    <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> VertexEndcapCollection </parameter>
    <parameter name="SimTrkHitRelCollection" type="string" lcioOutType="LCRelation"> VETrackerHitsRelations </parameter>
    <parameter name="TrackerHitCollectionName" type="string" lcioOutType="TrackerHitPlane"> VETrackerHits </parameter>
    <parameter name="ResolutionT" type="FloatVec"> 0.03  </parameter>
    <parameter name="UseTimeWindow" type="bool"> true </parameter>
    <parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
    <parameter name="TimeWindowMin" type="float"> -0.09 </parameter>
    <parameter name="TimeWindowMax" type="float"> 0.15 </parameter>
    <parameter name="Verbosity" type="string"> WARNING </parameter>
  </processor>

  <processor name="InnerPlanarDigiProcessor" type="DDPlanarDigiProcessor">
    <parameter name="SubDetectorName" type="string"> InnerTrackers </parameter>
    <parameter name="IsStrip" type="bool"> false </parameter>
    <parameter name="ResolutionU" type="float"> 0.007 </parameter>
    <parameter name="ResolutionV" type="float"> 0.090 </parameter>
    <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> InnerTrackerBarrelCollection </parameter>
    <parameter name="SimTrkHitRelCollection" type="string" lcioOutType="LCRelation"> IBTrackerHitsRelations </parameter>
    <parameter name="TrackerHitCollectionName" type="string" lcioOutType="TrackerHitPlane"> IBTrackerHits </parameter>
    <parameter name="ResolutionT" type="FloatVec"> 0.06  </parameter>
    <parameter name="UseTimeWindow" type="bool"> true </parameter>
    <parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
    <parameter name="TimeWindowMin" type="float"> -0.18 </parameter>
    <parameter name="TimeWindowMax" type="float"> 0.3 </parameter>
    <parameter name="Verbosity" type="string"> WARNING </parameter>
  </processor>

  <processor name="InnerEndcapPlanarDigiProcessor" type="DDPlanarDigiProcessor">
    <parameter name="SubDetectorName" type="string"> InnerTrackers </parameter>
    <parameter name="IsStrip" type="bool"> false </parameter>
    <parameter name="ResolutionU" type="float"> 0.007 </parameter>
    <parameter name="ResolutionV" type="float"> 0.090 </parameter>
    <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> InnerTrackerEndcapCollection </parameter>
    <parameter name="SimTrkHitRelCollection" type="string" lcioOutType="LCRelation"> IETrackerHitsRelations </parameter>
    <parameter name="TrackerHitCollectionName" type="string" lcioOutType="TrackerHitPlane"> IETrackerHits </parameter>
    <parameter name="ResolutionT" type="FloatVec"> 0.06  </parameter>
    <parameter name="UseTimeWindow" type="bool"> true </parameter>
    <parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
    <parameter name="TimeWindowMin" type="float"> -0.18 </parameter>
    <parameter name="TimeWindowMax" type="float"> 0.3 </parameter>
    <parameter name="Verbosity" type="string"> WARNING </parameter>
  </processor>

  <processor name="OuterPlanarDigiProcessor" type="DDPlanarDigiProcessor">
    <parameter name="SubDetectorName" type="string"> OuterTrackers </parameter>
    <parameter name="IsStrip" type="bool"> false </parameter>
    <parameter name="ResolutionU" type="float"> 0.007 </parameter>
    <parameter name="ResolutionV" type="float"> 0.090 </parameter>
    <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> OuterTrackerBarrelCollection </parameter>
    <parameter name="SimTrkHitRelCollection" type="string" lcioOutType="LCRelation"> OBTrackerHitsRelations </parameter>
    <parameter name="TrackerHitCollectionName" type="string" lcioOutType="TrackerHitPlane"> OBTrackerHits </parameter>
    <parameter name="ResolutionT" type="FloatVec"> 0.06  </parameter>
    <parameter name="UseTimeWindow" type="bool"> true </parameter>
    <parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
    <parameter name="TimeWindowMin" type="float"> -0.18 </parameter>
    <parameter name="TimeWindowMax" type="float"> 0.3 </parameter>
    <parameter name="Verbosity" type="string"> WARNING </parameter>
  </processor>

  <processor name="OuterEndcapPlanarDigiProcessor" type="DDPlanarDigiProcessor">
    <parameter name="SubDetectorName" type="string"> OuterTrackers </parameter>
    <parameter name="IsStrip" type="bool"> false </parameter>
    <parameter name="ResolutionU" type="float"> 0.007 </parameter>
    <parameter name="ResolutionV" type="float"> 0.090 </parameter>
    <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> OuterTrackerEndcapCollection </parameter>
    <parameter name="SimTrkHitRelCollection" type="string" lcioOutType="LCRelation"> OETrackerHitsRelations </parameter>
    <parameter name="TrackerHitCollectionName" type="string" lcioOutType="TrackerHitPlane"> OETrackerHits </parameter>
    <parameter name="ResolutionT" type="FloatVec"> 0.06  </parameter>
    <parameter name="UseTimeWindow" type="bool"> true </parameter>
    <parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
    <parameter name="TimeWindowMin" type="float"> -0.18 </parameter>
    <parameter name="TimeWindowMax" type="float"> 0.3 </parameter>
    <parameter name="Verbosity" type="string"> WARNING </parameter>
  </processor>

  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  <!-- == Track finding configuration                                                                        == -->
  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->

  <!-- == CONFORMAL TRACKING parameters == -->

  <processor name="ConformalTrackingILC" type="ConformalTrackingV2">
    <!--ConformalTracking constructs tracks using a combined conformal mapping and cellular automaton approach.-->
    <!--Name of the TrackerHit input collections-->
    <parameter name="TrackerHitCollectionNames" type="StringVec" lcioInType="TrackerHitPlane">
      VBTrackerHits
      IBTrackerHits
      OBTrackerHits
      VETrackerHits
      IETrackerHits
      OETrackerHits
    </parameter>
    <!--Name of the MCParticle input collection-->
    <parameter name="MCParticleCollectionName" type="string" lcioInType="MCParticle">MCParticle </parameter>
    <!--Name of the TrackerHit relation collections-->
    <parameter name="RelationsNames" type="StringVec" lcioInType="LCRelation">
      VBTrackerHitsRelations
      IBTrackerHitsRelations
      OBTrackerHitsRelations
      VETrackerHitsRelations
      IETrackerHitsRelations
      OETrackerHitsRelations
    </parameter>
    <!--Silicon track Collection Name-->
    <parameter name="SiTrackCollectionName" type="string" lcioOutType="Track">SiTracksCT </parameter>
    <!--Debug hits Collection Name-->
    <parameter name="DebugHits" type="string" lcioOutType="TrackerHitPlane"> DebugHits </parameter>
    <!--Maximum number of track hits to try the inverted fit-->
    <parameter name="MaxHitInvertedFit" type="int">4 </parameter>
    <!--Final minimum number of track clusters-->
    <parameter name="MinClustersOnTrackAfterFit" type="int">4 </parameter>
    <!--enable debug timing -->
    <parameter name="DebugTiming" type="bool">true </parameter>
    <!--enable debug plots -->
    <parameter name="DebugPlots" type="bool">true </parameter>
    <!--retry with tightened parameters, when too many tracks are being created-->
    <parameter name="RetryTooManyTracks" type="bool">false </parameter>
    <parameter name="TooManyTracks" type="int">500000 </parameter>
    <!--sort results from kdtree search-->
    <parameter name="SortTreeResults" type="bool">true </parameter>

    <parameter name="Steps" type="StringVec">
      [VXDBarrel]
      @Collections : VBTrackerHits
      @Parameters : MaxCellAngle : 0.005; MaxCellAngleRZ : 0.005; Chi2Cut : 100; MinClustersOnTrack : 4; MaxDistance : 0.02; SlopeZRange: 10.0; HighPTCut: 10.0;
      @Flags : HighPTFit, VertexToTracker
      @Functions : CombineCollections, BuildNewTracks
      [VXDEncap]
      @Collections : VETrackerHits
      @Parameters : MaxCellAngle : 0.005; MaxCellAngleRZ : 0.005; Chi2Cut : 100; MinClustersOnTrack : 4; MaxDistance : 0.02; SlopeZRange: 10.0; HighPTCut: 0.0;
      @Flags : HighPTFit, VertexToTracker
      @Functions : CombineCollections, ExtendTracks
      [LowerCellAngle1]
      @Collections : VBTrackerHits, VETrackerHits
      @Parameters : MaxCellAngle : 0.025; MaxCellAngleRZ : 0.025; Chi2Cut : 100; MinClustersOnTrack : 4; MaxDistance : 0.02; SlopeZRange: 10.0; HighPTCut: 10.0;
      @Flags : HighPTFit, VertexToTracker, RadialSearch
      @Functions : CombineCollections, BuildNewTracks
      [LowerCellAngle2]
      @Collections :
      @Parameters : MaxCellAngle : 0.05; MaxCellAngleRZ : 0.05; Chi2Cut : 2000; MinClustersOnTrack : 4; MaxDistance : 0.02; SlopeZRange: 10.0; HighPTCut: 10.0;
      @Flags : HighPTFit, VertexToTracker, RadialSearch
      @Functions : BuildNewTracks, SortTracks
      [Tracker]
      @Collections : IBTrackerHits, OBTrackerHits, IETrackerHits, OETrackerHits
      @Parameters : MaxCellAngle : 0.05; MaxCellAngleRZ : 0.05; Chi2Cut : 2000; MinClustersOnTrack : 4; MaxDistance : 0.02; SlopeZRange: 10.0; HighPTCut: 0.0;
      @Flags : HighPTFit, VertexToTracker, RadialSearch
      @Functions : CombineCollections, ExtendTracks
      [Displaced]
      @Collections : VBTrackerHits, VETrackerHits, IBTrackerHits, OBTrackerHits, IETrackerHits, OETrackerHits
      @Parameters : MaxCellAngle : 0.05; MaxCellAngleRZ : 0.05; Chi2Cut : 1000; MinClustersOnTrack : 5; MaxDistance : 0.015; SlopeZRange: 10.0; HighPTCut: 10.0;
      @Flags : OnlyZSchi2cut, RadialSearch
      @Functions : CombineCollections, BuildNewTracks
    </parameter>

    <parameter name="trackPurity" type="double">0.7 </parameter>
    <parameter name="ThetaRange" type="double"> 0.05 </parameter>
    <!--verbosity level of this processor ("DEBUG0-4,MESSAGE0-4,WARNING0-4,ERROR0-4,SILENT")-->
    <parameter name="Verbosity" type="string">DEBUG7</parameter>
  </processor>

  <!-- == TruthTrackFinder parameters == -->

  <processor name="MyTruthTrackFinder" type="TruthTrackFinder">
    <parameter name="FitForward" type="bool">true</parameter>
    <!--Define input tracker hits and relations. NB. Order must be respected -->
    <parameter name="TrackerHitCollectionNames" type="StringVec" lcioInType="TrackerHitPlane">
      VBTrackerHits
      IBTrackerHits
      OBTrackerHits
      VETrackerHits
      IETrackerHits
      OETrackerHits
    </parameter>
    <parameter name="SimTrackerHitRelCollectionNames" type="StringVec" lcioInType="LCRelation">
      VBTrackerHitsRelations
      IBTrackerHitsRelations
      OBTrackerHitsRelations
      VETrackerHitsRelations
      IETrackerHitsRelations
      OETrackerHitsRelations
    </parameter>
    <!--Name of the MCParticle input collection-->
    <parameter name="MCParticleCollectionName" type="string" lcioInType="MCParticle">MCParticle </parameter>
    <!--Silicon track Collection Name-->
    <parameter name="SiTrackCollectionName" type="string" lcioOutType="Track">SiTracks </parameter>
    <!--Silicon track particle relation Collection Name-->
    <parameter name="SiTrackRelationCollectionName" type="string" lcioOutType="LCRelation">SiTrackRelations </parameter>
    <!--If true use the truth information to initialise the helical prefit, otherwise use prefit by fitting 3 hits-->
    <parameter name="UseTruthInPrefit" type="bool">false </parameter>
    <parameter name="Verbosity" type="string">SILENT </parameter>
  </processor>

  <processor name="ClonesAndSplitTracksFinder" type="ClonesAndSplitTracksFinder">
    <parameter name="InputTrackCollectionName" type="string"> SiTracksCT </parameter>
    <parameter name="OutputTrackCollectionName" type="string"> SiTracks </parameter>
    <parameter name="MultipleScatteringOn" type="bool"> true </parameter>
    <parameter name="EnergyLossOn" type="bool"> true </parameter>
    <parameter name="SmoothOn" type="bool"> false </parameter>
    <parameter name="extrapolateForward" type="bool"> true </parameter>
    <parameter name="maxDeltaTheta" type="double"> 0.59 </parameter>
    <parameter name="maxDeltaPhi" type="double"> 0.99 </parameter>
    <parameter name="maxDeltaPt" type="double"> 0.69 </parameter>
    <parameter name="mergeSplitTracks" type="bool"> false </parameter>
  </processor>

  <processor name="Refit" type="RefitFinal">
    <!--Refit processor that calls finaliseLCIOTrack after taking the trackstate from the existing track. No re-sorting of hits is done-->
    <!--Use Energy Loss in Fit-->
    <parameter name="EnergyLossOn" type="bool"> true </parameter>
    <!--Name of the input track to MCParticle relation collection-->
    <parameter name="InputRelationCollectionName" type="string" lcioInType="LCRelation"> SiTrackRelations </parameter>
    <!--Name of the input track collection-->
    <parameter name="InputTrackCollectionName" type="string" lcioInType="Track"> SiTracks </parameter>
    <!--maximum allowable chi2 increment when moving from one site to another-->
    <parameter name="Max_Chi2_Incr" type="double"> 1.79769e+30 </parameter>
    <!--Use MultipleScattering in Fit-->
    <parameter name="MultipleScatteringOn" type="bool"> true </parameter>
    <!--Name of the output track collection-->
    <parameter name="OutputTrackCollectionName" type="string" lcioOutType="Track">
      SiTracks_Refitted
    </parameter>
    <!--Identifier of the reference point to use for the fit initialisation, -1 means at 0 0 0-->
    <parameter name="ReferencePoint" type="int"> -1 </parameter>
    <!--Smooth All Mesurement Sites in Fit-->
    <parameter name="SmoothOn" type="bool"> false </parameter>
    <!--verbosity level of this processor ("DEBUG0-4,MESSAGE0-4,WARNING0-4,ERROR0-4,SILENT")-->
    <!--parameter name="Verbosity" type="string"> DEBUG </parameter-->
    <!--if true extrapolation in the forward direction (in-out), otherwise backward (out-in)-->
    <parameter name="extrapolateForward" type="bool"> true </parameter>
    <!--Final minimum number of track clusters-->
    <parameter name="MinClustersOnTrackAfterFit" type="int">3 </parameter>
  </processor>


  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->
  <!-- == LCTuple configuration                                                                              == -->
  <!-- ======================================================================================================== -->
  <!-- ======================================================================================================== -->

  <processor name="MergeHits" type="MergeCollections">
    <parameter name="InputCollections" type="StringVec">
      VBTrackerHits
      IBTrackerHits
      OBTrackerHits
      VETrackerHits
      IETrackerHits
      OETrackerHits
    </parameter>
    <parameter name="OutputCollection" type="string"> HitsCollection </parameter>
  </processor>

  <processor name="MyCreateTrackRelations" type="CreateTrackRelations">
    <parameter name="TrackCollection" type="string">SiTracks_Refitted</parameter>
    <parameter name="Track2TrackerHitRelationName" type="string">SiTracks_Refitted_HitsRelation</parameter>
  </processor>

  <processor name="MyTrackTruth" type="TrackTruthProc">
    <parameter name="TrackCollection" type="string" lcioInType="Track">SiTracks_Refitted</parameter>
    <parameter name="MCParticleCollection" type="string" lcioInType="MCParticle">MCParticle</parameter>
    <parameter name="TrackerHit2SimTrackerHitRelationName" type="StringVec" lcioInType="LCRelation">
      VBTrackerHitsRelations
      IBTrackerHitsRelations
      OBTrackerHitsRelations
      VETrackerHitsRelations
      IETrackerHitsRelations
      OETrackerHitsRelations
    </parameter>
    <parameter name="Particle2TrackRelationName" type="string" lcioOutType="LCRelation">MCParticle_SiTracks_Refitted</parameter>
  </processor>

  <processor name="MyLCParquet" type="TrackPerfParquet">
    <!--Location of output-->
    <parameter name="OutputDir" type="string">data_conformal</parameter>
    <!--Name of the MCParticle collection-->
    <parameter name="MCParticleCollection" type="string" lcioInType="MCParticle">MCParticle</parameter>
    <!--Name of the Track collection-->
    <parameter name="TrackCollection" type="string" lcioInType="Track">SiTracks_Refitted</parameter>
    <!--Relations-->
    <parameter name="MCTrackRelationCollection" type="string" lcioInType="LCRelation">MCParticle_SiTracks_Refitted</parameter>
  </processor>

</marlin>
